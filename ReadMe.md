# **A Java EE Blog Site Project** #
## CST-235 Red Group 
Janurary 7 - February 24th 2019

---
## **About**
This is a group project for CST-235 at Grand Canyon University. The purpose of this project is to generate a Java EE application that provides a user interface to access and manipulate data stored on a server. Generation of the project is spread across 6 milestones covering 7 weeks.

## **Milestone 1**
### **Requirements**
* Submit a project proposal for a application based on Java EE. 
    * Red Group's proposal is to create a blog site providing tech news. The site will allow posting of new blogs and access/manipulation of stored blogs.
* Complete the first team group report.

### **Files**
#### **\Milestone1 Directory**
RedGroupProjectProposal \- Inital proposal for the Red Group blog project  
RedGroupReport \- Inital Red Group project report

## **Contributors**
Name | Email
-----|-------
Shawn Fradet | <Sfradet@my.gcu.edu>
Ray Omoregie | <ROmoregie1@my.gcu.edu>
Phillip Radke | <PRadke@my.gcu.edu>
Mercedes Thigpen | <MThigpen@my.gcu.edu>
